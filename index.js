const express = require('express');
const app = express();
const Promise = require('promise');
const request = require('request');

app.get('/cots2', function(req, res){
    var a = req.query.a;
	var b = req.query.b;
	var c = req.query.c;
	var d = req.query.d;

	var invalid = checkValidArguments(a, b, c, d);
	if (invalid) {
		return res.json(invalid).status(400);
    }

    Promise.all([
        r1(a, b, c, d),
        r2(a, b, c, d),
        r3(a, b, c, d)
    ]).then(function(data){
        return Promise.all([
            add(data[0].result, data[1].result),
            new Promise(function(resolve, reject){ resolve(data[2].result) })
        ]);
    }).then(function(data){
        return subtract(data[0].result, data[1])
    }).then(function(data){
        return res.json({
            'status': 'success',
            'result': data.result
        });
    }).catch(function(err) {
		res.json({
			'status': 'error',
			'message': err
		}).status(500);
	});
});

function r1(a, b, c, d) {
    console.log('Calculating R1...');
	var options = {
		'url': 'http://host21021.proxy.infralabs.cs.ui.ac.id/cots2/r1',
		'qs': {
			'a': a,
            'b': b,
            'c': c,
            'd': d
		}
	};

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with R1 operation.');
			}
    
			console.log('R1 finished: ' + JSON.parse(body).result);
			return resolve({'result': JSON.parse(body).result});
		});
	});
}

function r2(a, b, c, d) {
    console.log('Calculating R2...');
	var options = {
		'url': 'http://host22021.proxy.infralabs.cs.ui.ac.id/cots2/r2',
		'qs': {
			'a': a,
            'b': b,
            'c': c,
            'd': d
		}
	};

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with R2 operation.');
			}
    
			console.log('R2 finished: ' + JSON.parse(body).result);
			return resolve({'result': JSON.parse(body).result});
		});
	});
}

function r3(a, b, c, d) {
    console.log('Calculating R3...');
	var options = {
		'url': 'http://host23021.proxy.infralabs.cs.ui.ac.id/cots2/r3',
		'qs': {
			'a': a,
            'b': b,
            'c': c,
            'd': d
		}
	};

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with R3 operation.');
			}
    
			console.log('R3 finished: ' + JSON.parse(body).result);
			return resolve({'result': JSON.parse(body).result});
		});
	});
}

function add(a, b) {
	console.log('Adding... (' + a + ' + ' + b + ')');
	var options = {
		'url': 'http://host20099.proxy.infralabs.cs.ui.ac.id/tambah.php',
		'qs': {
			'a': a,
			'b': b
		}
	};

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with ADD operation.');
			}
	
			console.log('ADD finished: ' + a + ' + ' + b + ' = ' + JSON.parse(body).hasil);
			return resolve({'result': JSON.parse(body).hasil});
		});

		setTimeout(function() {
            reject('Timeout: No response from ADD operation after 1 minute.');
        }, 600000);
	});
}

function subtract(a, b) {
	console.log('Subtracting... (' + a + ' - ' + b + ')');
	var options = {
		'method': 'POST',
		'url': 'http://host20099.proxy.infralabs.cs.ui.ac.id/kurang.php',
		'form': {
			'a': a,
			'b': b
		}
	}

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with SUBTRACT operation.');
			}
	
			console.log('SUBTRACT finished: ' + a + ' - ' + b + ' = ' + body);
			return resolve({'result': JSON.parse(body)});
		});

		setTimeout(function() {
            reject('Timeout: No response from SUBTRACT operation after 1 minute.');
        }, 600000);
	});
}

function checkValidArguments(a, b, c, d) {
	var args = ['a', 'b', 'c', 'd'];
	var nanArray = [isNaN(a), isNaN(b), isNaN(c), isNaN(d)];
	var message = 'Error: The argument ';
	var exist = false;
	for (var i = 0; i < nanArray.length; i++) {
		if (nanArray[i]) {
			if (exist) {
				message += ', ';
			}
			message += args[i];
			exist |= nanArray[i];
		}
	}
	message += ' is not a number.';

	if (exist) {
		return {
			'status': 'error',
			'message': message
		}
	}

	return false;
}

app.listen(9011, function(){
    console.log('COTS 2 is running on port 9011...');
});